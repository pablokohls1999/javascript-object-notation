
/*ALGORITMO QUE CALCULE LO QUE GANA UN EMPLEADO CON BASE A LAS HORAS TRABAJADAS TENIENDO EN CUENTA QUE CADA HORA
SE PAGA A 2000, ADICIONALMENTE SE LE REALIZA UNOS DESCUENTO CON RESPECTO A UN IMPUESTO DE SEGURIDAD DEL 10% SOBRE SU SALARIO,
EL SISTEMA DEBE IMPRIMIR UN MENSAJE INDICANDO EL NOMBRE DEL EMPLEADO Y SU SUELDO TOTAL*/

let nombre_Empleado = 'Juan';
let h_Trabajadas = 50;
let impuesto = 0.10;
let pago_Hora, salario, v_Impuesto;

pago_Hora = 2000*h_Trabajadas;
v_Impuesto = pago_Hora* impuesto;
salario = pago_Hora - v_Impuesto;

console.log(`El nombre del empleado es ${nombre_Empleado} y el sueldo es de: ${salario} dólares` );