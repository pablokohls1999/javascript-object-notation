
/*ESCRIBA UN ALGORITMO QUE, DADO EL NUMERO DE HORAS TRABAJADAS POR UN EMPLEADO Y EL SUELDO POR HORA, CALCULE EL SUELDO TOTAL
DE ESE EMPLEADO. TENGA EN CUENTA QUE LAS HORAS EXTRAS SE PAGAN EL DOBLE*/

let sueldo;
let h_Trabajadas = 20;
let v_Por_Hora = 5;
let h_Extras = 0;
let sueldo_Total;
let V_H_Extras;

sueldo = v_Por_Hora* h_Trabajadas;
V_H_Extras =  v_Por_Hora * h_Extras * 2;
sueldo_Total = sueldo + V_H_Extras;

console.log(`El sueldo total de este empleado es de ${sueldo_Total}`);
