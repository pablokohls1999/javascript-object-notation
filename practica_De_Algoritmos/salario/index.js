
/*CALCULAR EL NUEVO SALARIO DE UN EMPLEADO SI OBTUVO UN INCREMENTO DEL 8% SOBRE SU SALARIO ACTUAL Y UN DESCUENTO 
DE 2.5% POR SERVICIOS*/

let salario = 5000;
//let incremento;
//let s_con_Incremento;
let v_Incremento = 0.08;
//let descuento;
let v_Descuento = 0.025;
//let s_Total;

incremento =salario*v_Incremento;
s_con_Incremento = salario + incremento;
descuento = s_con_Incremento*v_Descuento;
s_Total = s_con_Incremento - descuento;

console.log( `El sueldo de este empleado es de: ${s_Total}`);
