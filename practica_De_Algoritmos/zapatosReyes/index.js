
/*LA TIENDA MIS ZAPATOS ESTÁ DE ANIVERSARIO, POR ESA RAZON SE ESTÁ REALIZANDO UN DESCUENTO DEL 50% EN COMPRAS MAYORES
O IGUALES A 100 Y UN DESCUENTO DEL 10% EN COMPRAS NORMALES, HAGA UN ALGORITMO QUE PERMITA VALIDAR CUANDO SE DEBE APLICAR
EL DESCUENTO A UN CLIENTE, SE DEBE INFORMAR AL USUARIO INDICANDO EL VALOR DESCONTADO*/

let compra = 5;
let d_Obtenido, t_Compra;

if (compra>=100) {
    let descuento = 0.5;
    t_Compra= compra*descuento;
    d_Obtenido = t_Compra;

    
} else {
    let descuento = 0.10;
    d_Obtenido=compra*descuento;
    t_Compra= compra-d_Obtenido;
}
console.log(`Usted ha tenido un descuento de: ${d_Obtenido}`);
console.log(`El valor final de su compra es de: ${t_Compra}`);