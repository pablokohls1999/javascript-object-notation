const express = require('express');
const cors = require('cors');
const cookiesession = require('cookie-session');
const db = require('./app/models');
const { Model } = require('sequelize');

//REQUIRE REQUIRE REUIQYRE REQUIRE
 const app = express();


 app.use(cors());

 app.use(express.json());

 const Role = db.role;

 db.sequelize.sync();

 /*db.sequelize.sync({force: true}).then(() =>{
    console.log('drop and resync DB');
    initial();
 });*/


 app.use(express.urlencoded({extended: true}));

 app.use(
    cookiesession({
        name: 'Pablo-Kohls',
        secret: 'COOKIE_SECRET',
        httpOnly: true
    })
 );

 //simple route
app.get('/', (req, res)=>{
    res.json({message: 'Welcome a la aplicacion'})
});

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, ( ) =>{
    console.log(`sERVER IS RUNNIG ON PORT ${PORT}`);
});

function initial() {
    Role.create({
        id: 1,
        name: 'user'
    });
    Role.create({
        id: 2,
        name: 'moderator'
    });
    Role.create({
        id: 3,
        name: 'admin'
    });
 }
 
