//crear objetos

const users =
{
    name: "bryan",
    lastname: "kohls",
    age: 20,
    nickname: "bryan123",
    hobbies: ("run", "code", "wai"),    
    address: {
        street: "calle 28",
        city: "Manta",
    },
    married: true,
    //funcion
    greet(){
        return 'hello'
    },
    something(){

    }
}
    //arreglo
    const amigos = [
        {name: "steven", nickname: "joe123"},
        {name: "marcos", nickname: "marcos123"},
        {name: "juan", nickname: "juan123"},
        
    ]

    users.amigos = amigos

    let output = ''

    //recorrido con bucle for
    for (let index = 0; index < amigos.length; index++) {
        output = output + `<li>${amigos[index].name} ${amigos[index].nickname}</li>`
        
    }

   
    document.getElementById('people').innerHTML = output 
 

console.log(JSON.stringify(users));
//console.log(users.greet());
//console.log(users);
//users es un objeto pero lo queremos convertir a un json con la siguiente linea
//console.log(JSON.stringify(users));//stringify convierte de string a un json valido